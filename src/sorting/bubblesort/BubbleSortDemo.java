package sorting.bubblesort;

public class BubbleSortDemo {
    public static void main(String[] args) {
        BubbleSortDemo bubbleSortDemo=new BubbleSortDemo();
        bubbleSortDemo.bubbleSort();
    }

    public void bubbleSort(){
        int[] arr={5,1,9,2,10};
        boolean isSwapped;
        int n=arr.length;
        for (int i=0;i<n-1;i++){
            isSwapped=false;
            for (int j=0;j<n-1-i;j++){
                if (arr[j]>arr[j+1]){
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                    isSwapped=true;
                }
            }
            if (isSwapped==false){
                for (int el : arr){
                    System.out.println(el);
                }
                break;
            }
        }
    }

}
