package arrays;

public class MoveZeroToLast {

    public static void main(String[] args) {
        Integer[] numArray = {0,4,5,7,0,2,8,0,1};
        Integer start=0;
        Integer end = numArray.length-1;
        Integer[] tempArray = new Integer[numArray.length];

        for (Integer a: numArray){
            if (a==0){
                tempArray[end]=a;
                end--;
            }else {
                tempArray[start] = a;
                start++;
            }
        }
        ArrayUtils arrayUtils=new ArrayUtils();
        arrayUtils.printArray(tempArray,"move zero");
    }
}
