package arrays;

public class PalindromeCheck {
    public static void main(String[] args) {
        String word="madam";
        Boolean checkPalindrome = checkPalindrome(word);
        if (checkPalindrome){
            System.out.println("the word is palindrome");
        }else {
            System.out.println("the word is not palindrome");
        }

    }

    private static Boolean checkPalindrome(String word) {
        char[] characters=word.toCharArray();
        Integer start=0;
        Integer end=characters.length-1;

        for (int i=start;i<end;i++){
            if (characters[i]==characters[end]){
                end--;
            }else {
                return false;
            }
        }

        return true;
    }
}
