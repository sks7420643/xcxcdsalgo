package linkedlist.singly;

//insert a new node in a sorted linked list
public class InsertNodeSorted {
    private ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;
        
        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        InsertNodeSorted insertNodeSorted=new InsertNodeSorted();
        insertNodeSorted.head=new ListNode(2);
        ListNode second=new ListNode(3);
        ListNode third=new ListNode(4);
        ListNode fourth=new ListNode(6);
        ListNode fifth=new ListNode(7);
        ListNode sixth=new ListNode(8);

        insertNodeSorted.head.next=second;
        second.next=third;
        third.next=fourth;
        fourth.next=fifth;
        fifth.next=sixth;

        insertNodeSorted.display();
        insertNodeSorted.insert();
        insertNodeSorted.display();
    }

    private void insert() {
        ListNode newNode=new ListNode(5);
        ListNode temp=null;
        ListNode current=head;
        while (current!=null  && current.data<newNode.data){
            temp=current;
            current=current.next;
        }
        newNode.next=current;
        temp.next=newNode;
    }

    private void display() {
        ListNode current=head;
        System.out.println("start print");
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
    }
}
