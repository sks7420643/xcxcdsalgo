package linkedlist.singly;

public class CreateLinkedList {
    private ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;

        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        CreateLinkedList sll=new CreateLinkedList();
        sll.head=new ListNode(10);
        ListNode second=new ListNode(5);
        ListNode third=new ListNode(8);
        ListNode fourth=new ListNode(1);

        sll.head.next=second;
        second.next=third;
        third.next=fourth;

        sll.display();
        sll.findLength();
    }

    private void findLength() {
        int count=0;
        ListNode current=head;
        while (current!=null){
            current=current.next;
            count++;
        }

        System.out.println("the node length is :"+count);
    }

    private void display() {
        ListNode current=head;
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
    }
}
