package linkedlist.singly;

public class RemoveDuplicateFromSorted {
    private ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;

        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        RemoveDuplicateFromSorted removeDuplicateFromSorted=new RemoveDuplicateFromSorted();
        removeDuplicateFromSorted.head=new ListNode(1);
        ListNode second=new ListNode(1);
        ListNode third=new ListNode(2);
        ListNode fourth=new ListNode(3);
        ListNode fifth=new ListNode(3);
        ListNode sixth=new ListNode(4);

        removeDuplicateFromSorted.head.next=second;
        second.next=third;
        third.next=fourth;
        fourth.next=fifth;
        fifth.next=sixth;

        removeDuplicateFromSorted.display();
        removeDuplicateFromSorted.remove();
        removeDuplicateFromSorted.display();
    }

    private void remove() {
        if (head==null){
            return;
        }
        ListNode current=head;
        while (current!=null && current.next!=null){
            if (current.data==current.next.data){
                current.next=current.next.next;
            }else {
                current=current.next;
            }
        }

    }

    private void display() {
        ListNode current=head;
        System.out.println("start print");
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
    }
}
