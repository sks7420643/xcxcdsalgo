package linkedlist.singly;

public class InsertNodeMiddleLinkedList {
    private ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;

        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        InsertNodeMiddleLinkedList insertNodeMiddleLinkedList =new InsertNodeMiddleLinkedList();
        insertNodeMiddleLinkedList.head=new ListNode(5);
        ListNode second=new ListNode(10);
        ListNode third=new ListNode(15);
        ListNode fourth=new ListNode(20);

        insertNodeMiddleLinkedList.head.next=second;
        second.next=third;
        third.next=fourth;

        insertNodeMiddleLinkedList.display("Create a node");
//        insertNodeMiddleSinglyLinkedList.insertNodeMiddle(3);
        insertNodeMiddleLinkedList.insertNodeMiddleByMe(1);
    }

    private void insertNodeMiddleByMe(int position) {
        ListNode newNode=new ListNode(35);
        if (position==1){
            newNode.next=head;
            head=newNode;
        }else {
            ListNode current=head;
            ListNode after=null;
            int count=1;
            while (count<position-1){
                current=current.next;
                count++;
            }
            after=current.next;
            current.next=newNode;
            newNode.next=after;
        }
        display("after insert new node");
    }


    private void insertNodeMiddle(int position) {
        ListNode node=new ListNode(12);
        if (position==1){
            node.next=head;
            head=node;
            display("inserted in position:"+position);
        }else {
            ListNode previous=head;
            int count=1;
            while (count<position-1){
                previous=previous.next;
                count++;
            }
            ListNode current=previous.next;
            node.next=current;
            previous.next=node;
            display("inserted in position:"+position);
        }
    }

    private void display(String message) {
        ListNode current=head;
        System.out.println(message);
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
        System.out.println();
    }
}
