package linkedlist.singly;

public class SearchElementLinkedList {
    private ListNode head;
    private static class ListNode{
        private int data;
        private ListNode next;

        public ListNode(int data){
            this.data=data;
            this.next=null;
        }
    }

    public static void main(String[] args) {
        SearchElementLinkedList searchElementLinkedList=new SearchElementLinkedList();
        searchElementLinkedList.head=new ListNode(3);
        ListNode second=new ListNode(5);
        ListNode third=new ListNode(7);
        ListNode fourth=new ListNode(9);
        ListNode fifth=new ListNode(11);

        searchElementLinkedList.head.next=second;
        second.next=third;
        third.next=fourth;
        fourth.next=fifth;

        searchElementLinkedList.display();
        searchElementLinkedList.searchElement(50);
    }

    private void searchElement(int element) {
        ListNode current=head;
        int count=1;
        while (current!=null){
            if (current.data==element){
                System.out.println("element found at position:"+count);
                break;
            }
            current=current.next;
            count++;
        }
        System.out.println("element not found");
    }

    private void display() {
        ListNode current=head;
        while (current!=null){
            System.out.println(current.data);
            current=current.next;
        }
    }
}
